

/**
 * Returns the styling for console.error messages
 */
export function getErrorStyle() {
    return "background:red;color:white; padding-left:2px; padding-right:2px;"
}
