import {getParentDimensions} from "../utils";

/**
 * Create a new WebGL context
 * @param canvas {HTMLCanvasElement} an optional canvas element to pass in. If nothing is passed in a canvas is
 * automatically created
 * @param options {Object} various options for the context. If you need specific [WebGL2RenderingContext] options
 * pass in the object with the key "glOptions"
 */
export default function createContext(
    {
        canvas = document.createElement("canvas"),
        options = {
            width: window.innerWidth,
            height: window.innerHeight,
            glOptions: {
                alpha: true,
                antialias: true,
                depth: true
            }

        }
    } = {}): WebGL2RenderingContext {

    // build context
    let gl: WebGL2RenderingContext = (canvas.getContext("webgl2", options.glOptions)) as WebGL2RenderingContext;
    canvas.width = options.width;
    canvas.height = options.height;

    // extend GL prototypes
    for (let i in Context.prototype) {
        gl[i] = Context.prototype[i];
    }
    Object.assign(gl, new Context(options.width, options.height));

    return gl as WebGL2RenderingContext
}

/**
 * Extends an external WebGL2RenderingContext with additional properties like the default function
 * @param gl {WebGL2RenderingContext}
 */
export function extendContext(gl: WebGL2RenderingContext) {
    // extend GL prototypes
    for (let i in Context.prototype) {
        gl[i] = Context.prototype[i];
    }
    Object.assign(gl, new Context(gl.canvas.width, gl.canvas.height));

    return gl
}

/**
 * The base function prototype that will help add some additional functionality on top of [WebGL2RenderingContext]
 * @param width {number} the width of the canvas
 * @param height {number} the height of the canvas
 * @constructor
 */
function Context(width = this.canvas.width, height = this.canvas.height) {
    this.viewportX = 0;
    this.viewportY = 0;
    this.width = width;
    this.height = height;
}

Context.prototype = {

    /**
     * Appends canvas to specified element
     * @param el {HTMLElement} the element to append the canvas to.
     */
    appendTo(el: HTMLElement) {
        el.appendChild(this.canvas)
    },

    /**
     * Returns the current aspect ratio of the canvas.
     */
    getAspectRatio() {
        return this.canvas.width / this.canvas.height
    },

    /**
     * Sets the viewport for the context. All values are optional, if nothing is passed in this is the same as
     * running gl.viewport
     * @param x {number} x position
     * @param y {number} y position
     * @param width {number} width for viewport
     * @param height {number} height for viewport
     */
    updateViewport(x: number = 0, y: number = 0, width: number = this.canvas.width, height: number = this.canvas.height) {
        this.viewportX = x;
        this.viewportY = y;

        this.canvas.width = width;
        this.canvas.height = height;

        this.viewport(x, y, width, height);
        return this;
    },

    /**
     * Clears color buffer bit
     */
    clearColorBit() {
        this.clear(this.COLOR_BUFFER_BIT);
        return this;
    },

    /**
     * Clears depth buffer bit.
     */
    clearDepthBit() {
        this.clear(this.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Helper function to set clear color and clear screen in one go.
     * @param r {number} the red component
     * @param g {number} the green component
     * @param b {number} the blue component
     * @param a {number} the alpha component
     */
    clearScreen(r: number = 0, g: number = 0, b: number = 0, a: number = 1) {
        this.clearColor(r, g, b, a);
        this.viewport(this.viewportX, this.viewportY, this.canvas.width, this.canvas.height);
        this.clear(this.COLOR_BUFFER_BIT | this.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Same function as clearScreen but can clear with an array.
     * @param clearColor{Array} an array with exactly 4 values indicating r,g,b,a
     */
    clearScreenArrayColor(clearColor: [number, number, number, number]) {
        this.clearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
        this.viewport(this.viewportX, this.viewportY, this.canvas.width, this.canvas.height);
        this.clear(this.COLOR_BUFFER_BIT | this.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Enables depth testing
     */
    enableDepthTest() {
        this.enable(this.DEPTH_TEST)
    },

    /**
     * Disables depth testing
     */
    disableDepthTest() {
        this.enable(this.DEPTH_TEST)
        return this
    },

    disableBlend() {
        this.disable(this.BLEND)
        return this
    },

    /**
     * Enables additive blending.
     * Note - that it assumes depth testing is already turned off.
     */
    enableAdditiveBlending() {
        this.enable(this.BLEND);
        this.blendEquationSeparate(this.FUNC_ADD, this.FUNC_ADD);
        this.blendFuncSeparate(this.ONE, this.ONE, this.ONE, this.ONE);
    },

    /**
     * Enables depth write
     * @returns {GLUtils}
     */
    enableDepthWrite() {
        this.depthMask(true);
        return this;
    },

    /**
     * Enables alpha blending
     */
    enableAlphaBlending() {
        this.enable(this.BLEND);
        this.blendFunc(this.SRC_ALPHA, this.ONE_MINUS_SRC_ALPHA);
    },

    /**
     * Sets the size of the gl canvas
     * @param width {Number} Width that the canvas should be. Defaults to entire window
     * @param height { Number} Height that the canvas should be. Defaults to window.innerHeight
     * @returns {RendererFormat}
     */
    setSize(width = window.innerWidth, height = window.innerHeight) {
        this.canvas.width = width;
        this.canvas.height = height;
        return this;
    },

    /**
     * Helper function to unbind currently bound textures.
     */
    unbindTexture() {
        this.bindTexture(this.TEXTURE_2D, null);
        return this;
    },

    /**
     * Sets the context to be full screen of it's containing element.
     * @param {function} customResizeCallback specify an optional callback to deal with what happens when the screen re-sizes.
     * @returns {WebGLRenderingContext}
     */
    setFullScreen(customResizeCallback = null) {

        //set the viewport size
        this.updateViewport()

        if (customResizeCallback) {
            window.addEventListener("resize", () => {
                let dimensions = getParentDimensions(this.canvas)
                this.canvas.width = dimensions[0]
                this.canvas.height = dimensions[1];

                this.updateViewport(0, 0, this.canvas.width, this.canvas.height);

                customResizeCallback(dimensions)
            });
        } else {
            window.addEventListener("resize", () => {
                let dimensions = getParentDimensions(this.canvas)
                this.canvas.width = dimensions[0]
                this.canvas.height = dimensions[1];
                this.updateViewport(0, 0, this.canvas.width, this.canvas.height);
            });
        }
        return this;
    }
}

