import {getErrorStyle} from "../log";
import {isFloat} from "../math/core";
import {ShaderFormat} from "./interfaces";

/**
 * Creates a WebGLProgram
 * @param gl {WebGL2RenderingContext} the context to use
 * @param fmt {ShaderFormat} the [ShaderFormat] object describing the aspects of the shader.
 */
export default function createShader(gl: WebGL2RenderingContext, fmt: ShaderFormat) {

    // shader needs a format
    if (!fmt) {
        return
    }

    let shader = gl.createProgram();

    for (let i in Shader.prototype) {
        shader[i] = Shader.prototype[i];
    }

    let glsl = new Shader(gl, fmt);
    Object.assign(shader, glsl);

    shader.loadShader();


    return shader

}

function Shader(gl: WebGL2RenderingContext, fmt: ShaderFormat) {
    this.gl = gl
    this.uniforms = fmt.uniforms !== undefined ? fmt.uniforms : {}
    this.attributes = fmt.attributes !== undefined ? fmt.attributes : {};
    this.version = `#version 300 es \n`;
    this.fmt = fmt;
    this.name = fmt.name;

}

Shader.prototype = {

    loadShader(
        vertex: string = this.fmt.vertex,
        fragment: string = this.fmt.fragment
    ) {
        let gl = this.gl;

        // make sure version is fmtified
        vertex = vertex.search("#version") === -1 ? this.version + vertex : vertex;
        fragment = fragment.search("#version") === -1 ? this.version + fragment : fragment;

        let vShader = this._compileShader(gl.VERTEX_SHADER, vertex);
        let fShader = this._compileShader(gl.FRAGMENT_SHADER, fragment);

        // attach shaders
        gl.attachShader(this, vShader);
        gl.attachShader(this, fShader);

        // if transform feedback is being used, we need to parse varyings before linking program.
        this._parseTransformFeedback();

        // link program
        gl.linkProgram(this);

        gl.deleteShader(vShader);
        gl.deleteShader(fShader);

        if (!gl.getProgramParameter(this, gl.LINK_STATUS)) {
            console.error(`%c Could not initialize WebGLProgram ${this.name}`, getErrorStyle());
            throw ("Couldn't link shader program - " + gl.getProgramInfoLog(this));
        } else {

            this._parseActiveAttributes();
            this._parseActiveUniforms();

            return this;
        }

        // process uniforms if user passes in any
        if (this.format.uniforms.length > 0) {
            this.format.uniforms.forEach(itm => {
                this.uniforms[itm.name] = {
                    location: itm.location,
                    name: itm.name
                }
            })
        }

        // parse any attributes if the user passes any
        if (this.format.attributes.length > 0) {

            this.format.attributes.forEach(attrib => {
                this.attributes[attrib.name] = {
                    size: attrib.size,
                    name: attrib.name,
                    type: attrib.type,
                    location: attrib.location
                }
            })
        }
    },

    /**
     * Binds the shader
     */
    bind() {
        this.gl.useProgram(this);
    },

    /**
     * Deletes shader
     */
    delete() {
        this.gl.deleteProgram(this);
    },

    /**
     * Sets a vec2 uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {Array} a 2 component array
     */
    vec2(name: string | number, value: [number, number]) {
        if (this.uniforms[name] !== undefined) {
            this.gl.uniform2fv(this.uniforms[name].location, value);
        } else if (typeof name === "number") {
            this.gl.uniform2fv(name, value)
        }
        return this;
    },

    /**
     * Sets a vec3 uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {Array} a 3 component array
     */
    vec3(name: string, value: [number, number, number]) {
        if (this.uniforms[name] !== undefined) {
            this.gl.uniform3fv(this.uniforms[name].location, value);
        } else if (typeof name === "number") {
            this.gl.uniform3fv(name, value)
        }
        return this;
    },

    /**
     * Sets a vec4 uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {Array} a 4 component array
     */
    vec4(name: string, value: [number, number, number, number]) {
        if (this.uniforms[name] !== undefined) {
            this.gl.uniform4fv(this.uniforms[name].location, value);
        } else if (typeof name === "number") {
            this.gl.uniform4fv(name, value)
        }
        return this;
    },

    /**
     * Sets an int uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {number} value to set
     */
    int(name: string | number, value: number) {

        if (this.uniforms[name] !== undefined) {
            this.gl.uniform1i(this.uniforms[name].location, value);
        } else if (typeof name === "number") {
            this.gl.uniform1i(name, value)
        }
        return this;
    },

    /**
     * Sets a float uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {number} value to set
     */
    float(name: string | number, value: number) {
        if (this.uniforms[name] !== undefined) {
            this.gl.uniform1f(this.uniforms[name].location, value);
        } else if (typeof name === "number") {
            this.gl.uniform1f(name, value)
        }
        return this;
    },

    /**
     * Sets a mat4 uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {Array} a 16 component array
     */
    mat4(name: string | number, value: [
        number, number, number, number,
        number, number, number, number,
        number, number, number, number,
        number, number, number, number
    ]) {
        if (this.uniforms[name] !== undefined) {
            this.gl.uniformMatrix4fv(this.uniforms[name].location, false, value);
        } else if (typeof name === "number") {
            this.gl.uniformMatrix4fv(name, false, value);
        }
        return this;
    },

    /**
     * Sets a mat3 uniform on the shader.
     * @param name {string | number} the name or the uniform location of the uniform
     * @param value {Array} a 9 component array
     */
    mat3(name: string | number, value: [
        number, number, number,
        number, number, number,
        number, number, number
    ]) {

        if (this.uniforms[name] !== undefined) {
            this.gl.uniformMatrix3fv(this.uniforms[name].location, false, value);
        } else if (typeof name === "number") {
            this.gl.uniformMatrix3fv(name, false, value);
        }
        return this;
    },

    /**
     * A general catch-all for calling the correct uniform function. Will attempt to derive the correct function call.
     * @param name {string | number} name or location value of the uniform
     * @param value {*} value to send to the uniform .
     */
    uniform(name: string | number, value: any) {

        if (value instanceof Array || Object.prototype.toString.call(value.buffer) === "[object ArrayBuffer]") {
            switch (value.length) {
                case 2:
                    this.vec2(name, value);
                    break;
                case 3:
                    this.vec3(name, value);
                    break;
                case 9:
                    this.mat3(name, value);
                    break;
                case 16:
                    this.mat4(name, value);
                    break;
            }
        } else {
            if (isFloat(value)) {
                this.float(name, value);
            } else {
                this.int(name, value);
            }
        }
    },

    /**
     * Looks through shader and parses the attribute data of any currently in-use
     * uniform values
     */
    _parseActiveUniforms() {
        let gl = this.gl;

        // set uniforms and their locations (plus default values if specified)
        let numUniforms = gl.getProgramParameter(this, gl.ACTIVE_UNIFORMS);

        // first loop through and save all active uniforms.
        for (let i = 0; i < numUniforms; ++i) {
            let info = gl.getActiveUniform(this, i);
            let location = gl.getUniformLocation(this, info.name);

            this.uniforms[info.name] = {
                location: location,
                name: info.name
            };

        }

    },

    /**
     * Looks through shader and parses the attribute data of any currently in-use
     * attribute values
     */
    _parseActiveAttributes() {
        let gl = this.gl;

        let attribs = gl.getProgramParameter(this, gl.ACTIVE_ATTRIBUTES);

        for (let i = 0; i < attribs; ++i) {
            let attrib = gl.getActiveAttrib(this, i);
            let loc = gl.getAttribLocation(this, attrib.name);

            if (loc !== null) {
                this.attributes[attrib.name] = {
                    size: attrib.size,
                    name: attrib.name,
                    type: attrib.type,
                    location: loc
                };
            }
        }
    },

    /**
     * Helps set up transform feedback when the varyings array and feedback mode are set.
     * @private
     */
    _parseTransformFeedback() {
        if (this.fmt.varyings !== undefined &&
            this.fmt.varyings.length > 0 &&
            this.fmt.feedbackMode !== null) {
            this.setVaryings(this.fmt.varyings, this.fmt.feedbackMode);
        }
    },

    /**
     * Sets the varyings for Transform Feedback
     * @param varyings {Array} the varying names for transform feedback
     * @param mode {number} the constant value for the transform feedback mode.
     */
    setVaryings(varyings: Array<string>, mode: number) {
        this.gl.transformFeedbackVaryings(this, varyings, mode);
    },

    /**
     * Compiles a shader
     * @param type {number} the type of shader. gl.FRAGMENT, gl.VERTEX, etc
     * @param source {string} the source for the shader
     */
    _compileShader(type: number, source: string) {
        let gl = this.gl;
        let shader = gl.createShader(type);
        let shaderTypeName = ""

        // get the string name of the type of shader we're trying to compile.
        if (type === gl.FRAGMENT_SHADER) {
            shaderTypeName = "FRAGMENT"
        } else {
            shaderTypeName = "VERTEX";
        }

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {

            if (this.name) {
                console.error(`%c Error in ${shaderTypeName} shader compilation with "${this.name}" - ` + gl.getShaderInfoLog(shader) + `\n source was :\n ${source}`, getErrorStyle());
            } else {
                console.error(`%c Error in ${shaderTypeName} shader compilation with ${this.name} - ` + gl.getShaderInfoLog(shader), getErrorStyle());
                console.log(source)
            }

            // just halt execution if we run into an error.
            throw new Error("Shader error - see message");

        } else {
            return shader;
        }

    }
}
