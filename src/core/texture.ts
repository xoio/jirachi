import {TextureFormat} from "../core/interfaces"

export default function createTexture(gl, format: TextureFormat) {


    let tex = gl.createTexture();

    // create a new [TextureFormat] object if none is passed in.
    if (!format) {
        format = new TextureFormat(gl)
    }

    for (let i in GLTexture.prototype) {
        tex[i] = GLTexture.prototype[i];
    }

    let t = new GLTexture(gl, format);
    Object.assign(tex, t);

    tex._checkData();

    return tex;
}

/**
 * Base functions for extending the WebGL texture object
 * @param gl {WebGLRenderingContext} a WebGL Rendering Context
 * @param format {TextureFormat} a TextureFormat object describing the texture.
 * @constructor
 */
function GLTexture(gl, format: TextureFormat) {
    this.gl = gl;
    this.format = format;
    this.bound = false

    // If data is instance of image but is essentially null, store width/height of texture using width/height params
    // of format object, otherwise store image size instead
    if ((format.data instanceof Image) && format.data.width === 0 && format.data.height === 0) {
        this.width = format.width
        this.height = format.height
    } else if ((format.data instanceof Image) && format.data.width > 0 && format.data.height > 0) {
        this.width = format.data.width
        this.height = format.data.height
    } else {
        this.width = format.width
        this.height = format.height
    }

    // reference to the information to be stored within the texture.
    this.data = null;
}

GLTexture.prototype = {

    /**
     * Checks to see if any data was set on the format and then calls the appropriate loading function.
     * Generally only called on texture init.
     * TODO handle non-image data
     */
    _checkData() {
        if (!this.format.loadBlank) {
            if (this.format.data !== null && this.format.data !== void 0) {
                if (this.format.data instanceof Image && this.format.data.width > 0 && this.format.data.height > 0) {
                    this.loadImage(this.format.data);
                } else {
                    this.loadData(this.format.data);
                }
            }
        } else {
            this.loadBlankTexture()
        }
    },

    /**
     * Alias for gl.bindTexture.
     */
    bindTexture() {
        let gl = this.gl;
        this.bound = true
        gl.bindTexture(this.format.target, this);
    },

    /**
     * Alias for binding a null texture(essentially unbinding the previously bound texture)
     */
    unbindTexture() {
        let gl = this.gl;
        this.bound = false
        gl.bindTexture(this.format.target, null);
    },

    /**
     * Changes current active texture and binds it
     * @param index {number} the texture index to bind and activate on.
     */
    bind(index: number = 0) {
        let gl = this.gl;
        this.bound = true
        gl.activeTexture(gl[`TEXTURE${index}`]);
        gl.bindTexture(this.format.target, this);
        return this;
    },

    /**
     * Unbinds all currently bound textures
     */
    unbind() {
        this.bound = false
        this.gl.bindTexture(this.format.target, null);
        return this;
    },

    /**
     * Restructures texture so that it is treated as a depth texture.
     * @param settings {TextureFormat}
     */
    createDepthTexture(settings?: TextureFormat) {
        let gl = this.gl;

        gl.bindTexture(this.format.target, this);
        gl.texParameteri(this.format.target, gl.TEXTURE_MAG_FILTER, settings.magFilter);
        gl.texParameteri(this.format.target, gl.TEXTURE_MIN_FILTER, settings.minFilter);

        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_S, settings.wrapS);
        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_T, settings.wrapT);

        if (gl instanceof WebGLRenderingContext) {
            gl.texImage2D(this.format.target, 0, settings.depthType, settings.width, settings.height, 0, settings.depthType, gl.UNSIGNED_SHORT, null);
        } else {
            gl.texImage2D(this.format.target, 0, settings.depthType, settings.width, settings.height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
        }

        gl.bindTexture(this.format.target, null);

        return this;
    },

    loadBlankTexture() {
        this.loadData(null);
        return this;
    },

    /**
     * Loads some data as the texture's contents.
     * @param data {TypedArray} A typed array with content for the texture.
     */
    loadData(data) {
        let gl = this.gl;
        let options = this.format;

        gl.bindTexture(this.format.target, this);

        // set the image
        // known to work with random floating point data
        //gl.texImage2D(this.format.target,0,gl.RGBA16F,width,height,0,gl.RGBA,gl.FLOAT,data);
        gl.texImage2D(
            this.format.target,
            0,
            options.internalFormat,
            options.width,
            options.height,
            0,
            options.format,
            options.texelType,
            data
        );

        // set min and mag filters
        gl.texParameteri(this.format.target, gl.TEXTURE_MAG_FILTER, options.magFilter);
        gl.texParameteri(this.format.target, gl.TEXTURE_MIN_FILTER, options.minFilter);

        //set wrapping
        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_S, options.wrapS)
        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_T, options.wrapT)

        // generate mipmaps if necessary
        if (options.generateMipMaps) {
            gl.generateMipmap(this.format.target);
        }
        gl.bindTexture(this.format.target, null);

        return this;
    },

    /**
     * Loads an image as the texture data
     * @param img {Image} an Image object containing the image to load.
     */
    loadImage(image: HTMLImageElement) {
        let gl = this.gl;
        let options = this.format;
        this.format.width = image.width;
        this.format.height = image.height;

        // store reference to original element.
        this.data = image;


        gl.bindTexture(this.format.target, this);

        // if we need to flip texture
        if (this.format.flipY) {
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, options.flipY);
        }

        // set the image
        gl.texImage2D(this.format.target, this.format.level, this.format.internalFormat, image.width, image.height, 0, this.format.format, this.format.texelType, image);

        // set min and mag filters
        gl.texParameteri(this.format.target, gl.TEXTURE_MAG_FILTER, options.magFilter);
        gl.texParameteri(this.format.target, gl.TEXTURE_MIN_FILTER, options.minFilter)

        //set wrapping
        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_S, options.wrapS)
        gl.texParameteri(this.format.target, gl.TEXTURE_WRAP_T, options.wrapT)

        // generate mipmaps if necessary
        if (options.generateMipMaps) {
            gl.generateMipmap(this.format.target);
        }

        // ensure texture is unbound.
        gl.bindTexture(this.format.target, null);

        return this;
    },

    /**
     * Sets the min filter
     * @param filter {GLenum} the filter to use. If nothing is passed in will default to whatever is set on the
     * format object.
     */
    setMinFilter(filter: GLenum = this.format.minFilter) {
        let gl = this.gl
        if (!this.bound) {
            this.bindTexture()
        }
        gl.texParameteri(this.format.target, gl.TEXTURE_MIN_FILTER, filter)

    },

    /**
     * Sets the mag filter
     * @param filter {GLenum} the filter to use. If nothing is passed in will default to whatever is set on the
     * format object.
     */
    setMagFilter(filter: GLenum = this.format.magFilter) {
        let gl = this.gl
        if (!this.bound) {
            this.bindTexture()
        }
        gl.texParameteri(this.format.target, gl.TEXTURE_MAG_FILTER, filter)

    },

    resize(w, h) {
        let gl = this.gl;
        let options = this.format;
        this.format.width = w;
        this.format.height = h;
        this.width = w;
        this.height = h;

        this.bind();

        gl.texImage2D(
            this.format.target,
            0,
            options.internalFormat,
            w,
            h,
            0,
            options.format,
            options.texelType,
            this.format.data
        );

        // unbind texture. necessary in case FBO needs to be resized.
        gl.bindTexture(this.format.target, null);
        return this;
    }

};

