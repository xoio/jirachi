export default function createVAO(gl: WebGL2RenderingContext) {
    let hasVertexArray = gl.createVertexArray === undefined;
    let vao;

    if (!hasVertexArray) {
        vao = gl.createVertexArray();
    } else {
        console.error("Please use a browser that supports WebGL2")
        return
    }

    for (let i in VAO.prototype) {
        vao[i] = VAO.prototype[i];
    }

    Object.assign(vao, new VAO(gl));

    return vao
}


function VAO(gl: WebGL2RenderingContext) {
    this.gl = gl
}

VAO.prototype = {
    bind() {
        this.gl.bindVertexArray(this);
    },

    unbind() {
        this.gl.bindVertexArray(null);
    },

    /**
     * Turns the attribute instanced.
     * @param attribute {number} the attribute location that should be instanced.
     * @param divisor {number} A GLuint specifying the number of instances that will pass between updates of the generic attribute. Usually 1 is more than enough.
     */
    makeInstancedAttribute(attribute: number, divisor: number = 1) {
        let gl = this.gl;
        gl.vertexAttribDivisor(attribute, divisor);
        return this;
    },

    /**
     * @deprecated
     * @param idx
     */
    enable(idx = 0) {
        if (idx > this.gl.MAX_VERTEX_ATTRIBS || idx < 0) {
            console.log(`Attempt to enable attrib ${idx} which is outside the range of the max number of available attributes`)
            return
        }

        this.gl.enableVertexAttribArray(idx);
        return this;
    },

    /**
     * @deprecated
     * @param idx
     */
    disable(idx = 0) {
        if (idx > this.gl.MAX_VERTEX_ATTRIBS || idx < 0) {
            console.log(`Attempt to enable attrib ${idx} which is outside the range of the max number of available attributes`)
            return
        }
        this.gl.disableVertexAttribArray(idx);
        return this;
    },

    /**
     * Enables an attribute on the Vao
     * @param idx {number} a index value. Should not exceed the number of vertex attributes supported
     * by your graphics card.
     */
    enableAttrib(idx = 0) {
        if (idx > this.gl.MAX_VERTEX_ATTRIBS || idx < 0) {
            console.log(`Attempt to enable attrib ${idx} which is outside the range of the max number of available attributes`)
            return
        }

        this.gl.enableVertexAttribArray(idx);
        return this;
    },

    /**
     * Enables an attribute on the Vao
     * @param idx {number} a index value. Should not exceed the number of vertex attributes supported
     * by your graphics card.
     * TODO maybe add a check to insure index is less than max
     */
    disableAttrib(idx = 0) {
        if (idx > this.gl.MAX_VERTEX_ATTRIBS || idx < 0) {
            console.log(`Attempt to enable attrib ${idx} which is outside the range of the max number of available attributes`)
            return
        }
        this.gl.disableVertexAttribArray(idx);
        return this;
    },

    /**
     * Associates the contents of a Vbo with a vertex attribute
     * @param index {number} the index of the vertex attribute to assign data to.
     * @param size {number} A GLint specifying the number of blocks per vertex attribute. Must be 1, 2, 3, or 4. ie positions have a size of 3 for x,y,z
     * @param type {number} GLenum of the type of data the Vbo contains
     * @param normalized {number} A GLboolean specifying whether integer data values should be normalized into a certain range when being casted to a float.
     * @param stride {number} A GLsizei specifying the offset in bytes between the beginning of consecutive vertex attributes. Cannot be larger than 255. If stride is 0, the attribute is assumed to be tightly packed, that is, the attributes are not interleaved but each attribute is in a separate block, and the next vertex' attribute follows immediately after the current vertex.
     * @param offset {number} A GLintptr specifying an offset in bytes of the first component in the vertex attribute array. Must be a multiple of the byte length of type.
     */
    assignData(
        index: number = 0,
        size: number = 3,
        type: number = this.gl.FLOAT,
        normalized: GLboolean = this.gl.FALSE,
        stride: number = 0,
        offset: number = 0
    ) {
        this.gl.vertexAttribPointer(index, size, type, normalized, stride, offset);
        return this;
    }

}