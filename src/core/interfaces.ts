
/**
 * Defines the basic items involved in creating a shader.
 */
export class ShaderFormat {
    vertex: string = ""
    fragment: string = ""
    varyings?: Array<string> = []
    feedbackMode?: number = 0

    // a name to help identify the shader
    name?: string = ""

    // optional stuffs
    uniforms?: Array<ShaderUniform> = []
    attributes?: Array<ShaderAttrib> = []
}

/**
 * Defines a shader attribute
 */
interface ShaderAttrib {
    size: number
    name: string
    type: string
    location: number
}

/** Defines a shader uniform */
interface ShaderUniform {
    location: number
    name: string
}


/**
 * Helps to define the basic properties of a Texture.
 * Used between standard textures and FBOs.
 * This is defined as a class so that we can have some defaults
 */
export class TextureFormat {
    wrapS: number;
    wrapT: number;
    width: number = 512;
    height: number = 512;
    minFilter: number;
    magFilter: number;
    depthType: number;
    data: HTMLImageElement | ArrayBufferView;
    attachments: Array<any>;
    target: number;
    internalFormat: number;
    texelType: number;
    level: number;
    format: number;
    flipY: boolean;

    loadBlank: boolean = false

    // a way to help identify an FBO or texture.
    name: string = " ";

    constructor(
        {
            data = null,
            isDepth = false,
            internalFormat = 0,
            format = 0,
            texelType = 0,
            width = 512,
            height = 512
        }: {
            data?: HTMLImageElement | ArrayBufferView | null,
            isDepth?: boolean,
            internalFormat?: number,
            format?: GLenum,
            texelType?: GLenum,
            width?: number
            height?: number

        } = {}) {

        this.wrapS = 33071
        this.wrapT = 33071
        this.width = width
        this.height = height
        this.minFilter = 9728
        this.magFilter = 9728
        this.internalFormat = internalFormat !== 0 ? internalFormat : 6407
        this.format = format !== 0 ? format : this.internalFormat
        this.texelType = texelType !== 0 ? texelType : 5121
        this.level = 0
        this.flipY = true
        this.attachments = []
        this.data = data

        // option to load a blank random texture if no data is passed in. Generally false.
        this.loadBlank = false

        if (this.data !== null) {

            // if data is an image, set width/height to image size
            if (data instanceof Image &&
                data.width > 0 &&
                data.height > 0) {
                this.width = data.width
                this.height = data.height
            }

        } else {
            this.initBlank()
        }

        this.target = 3553

        return this;
    }

    /**
     * Helper to adjust texture format settings to be suitable for a floating point texture(aka a texture that uses
     * a Float32Array)
     */
    setFloatTextureType() {
        this.internalFormat = 34836 // gl.RGBA32F
        this.format = 6408 // gl.RGBA
        this.texelType = 5126 // gl.FLOAT
        return this
    }

    /** Toggles whether or not to init a blank texture */
    initBlank() {
        this.loadBlank = true
        return this
    }

    /**
     * Sets the size for the texture
     * @param w {number} width for the texture
     * @param h {number} height for the texture.
     */
    setSize(w: number, h: number) {
        this.width = w
        this.height = h
        return this
    }

    setWrap(s: number, t: number) {
        this.wrapS = s
        this.wrapT = t
        return this
    }

    setTarget(target: GLenum) {
        this.target = target
        return this
    }

    setTexelType(type: GLenum) {
        this.texelType = type
        return this
    }

    setFilter(min: GLenum, mag: GLenum) {
        this.minFilter = min
        this.magFilter = mag
        return this
    }

    setMinFilter(min: GLenum) {
        this.magFilter = min
        return this
    }

    setMagFilter(mag: GLenum) {
        this.magFilter = mag
        return this
    }
}

/**
 * Generates an object with basic working internalFormat and format settings for RGBA textures.
 *
 * @param ctx {WebGLRenderingContext} a WebGL2 rendering context.
 */
export function generateBasicRGBASettings(ctx: WebGL2RenderingContext) {
    return {
        format: ctx.RGBA,
        internalFormat: ctx.RGBA,
    }
}

/**
 * Basic interface for describing regular or indexed Geometry.
 * Ensures that the object has a set of vertices and indices if need-be.
 * It also makes assumptions about some datatypes, this may not be the best interface to use if you need more
 * fine-grained control over things.
 */
export interface Geometry {
    // defines vertex information in a piece of geometry. Note that these are automatically assumed to be Float32
    vertices: Array<number>;

    // defines the index information, if any, in a piece of geometry. Values are automatically assumed to be Uint16
    indices: Array<any>;

    // describes the uv information in a piece of geometry
    uvs: Array<any>;

    normals: Array<any>;

    // this defines a custom vertex size number, ie, are vertices xyz or xy when drawing 2d.
    customSize: number;
}

export interface Camera {
    projectionMatrix: Float32Array | any[]
    viewMatrix: Float32Array | any[]
    matrixWorld: Float32Array | any[]
    near: number
    far: number
    aspect: number
    fov: number
    eye: number[]
    target: number[]
    center: number[]
    position: number[]
    up: number[]

    // translate the camera eye
    translate: Function
}


export class Attribute {
    name: string = ""
    data: ArrayBufferView | WebGLBuffer
    size: number = 3
    normalized: boolean = false
    format: number = 0
    stride: number = 0
    offset: number = 0
    buffer?: WebGLBuffer

    constructor(
        name: string,
        data: ArrayBufferView | WebGLBuffer,
        {
            size = 3,
            normalized = false,
            format = 0,
            stride = 0,
            offset = 0
        } = {}
    ) {

        this.name = name
        this.data = data instanceof WebGLBuffer ? null : data
        this.buffer = data instanceof WebGLBuffer ? data : null
        this.size = size
        this.normalized = normalized
        this.format = format
        this.stride = stride
        this.offset = offset

    }

}
