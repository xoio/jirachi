export default function createVBO(
    gl: WebGL2RenderingContext,
    {
        indexed = false,
        data = null,
        usage = 35044 // default is gl.STATIC_DRAW

    }: {
        indexed?: boolean,
        data?: ArrayBufferView | WebGLBuffer,
        usage?: number
    } = {}) {

    let vbo = gl.createBuffer();

    if (indexed) {
        for (let i in IndexVbo.prototype) {
            vbo[i] = IndexVbo.prototype[i];
        }
        Object.assign(vbo, new IndexVbo(gl));

    } else {
        for (let i in Vbo.prototype) {
            vbo[i] = Vbo.prototype[i];
        }

        Object.assign(vbo, new Vbo(gl));
    }

    if (data !== undefined) {
        if (usage !== null) {
            vbo.bufferData(data, usage);
        } else {
            vbo.bufferData(data);
        }
    }

    return vbo;

}


/**
 * Functional definition for a regular VBO buffer
 * @param gl {WebGLRenderingContext} a WebGL context
 * @constructor
 */
function Vbo(gl: WebGL2RenderingContext) {
    this.gl = gl;
}

Vbo.prototype = {

    /**
     * Binds this buffer
     */
    bind() {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this);
    },

    /**
     * Unbinds previously bound buffer
     */
    unbind() {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
    },

    /**
     * Buffers data onto a VBO
     * @param data {ArrayBufferView} a TypedArray of information to load into the buffer
     * @param usage {number} GLenum indicating how the data will get used.
     */
    bufferData(
        data: ArrayBufferView,
        usage: number = this.gl.STATIC_DRAW
    ) {
        this.bind();
        this.gl.bufferData(this.gl.ARRAY_BUFFER, data, usage);
        this.unbind();

        return this;
    },

    /**
     * Updates the buffer (by running bufferSubData)
     * @param data {ArrayBufferView} TypedArray of data to update.
     * @param offset {number} offset value to use when setting data.
     */
    updateBuffer(data: ArrayBufferView, offset = 0) {

        this.bind();
        this.gl.bufferSubData(this.gl.ARRAY_BUFFER, offset, data);
        this.unbind();
        return this;
    }
};


/**
 * Functional definition for an indexed VBO
 * @param gl {WebGLRenderingContext} a WebGLRendering Context
 * @constructor
 */
function IndexVbo(gl: WebGL2RenderingContext) {
    this.gl = gl;
}

IndexVbo.prototype = {
    /**
     * Binds this buffer
     */
    bind() {
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this);
    },

    /**
     * Unbinds previously bound buffer
     */
    unbind() {
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, null);
    },

    /**
     * Buffers data onto a VBO
     * @param data {ArrayBufferView} a TypedArray of information to load into the buffer
     * @param usage {number} GLenum indicating how the data will get used.
     */
    bufferData(
        data: ArrayBufferView,
        usage: number = this.gl.STATIC_DRAW
    ) {
        this.bind();
        this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, data, usage);
        this.unbind();

        return this;
    },

    /**
     * Updates the buffer (by running bufferSubData)
     * @param data {ArrayBufferView} TypedArray of data to update.
     * @param offset {number} offset value to use when setting data.
     */
    updateBuffer(data: ArrayBufferView, offset = 0) {
        this.gl.bufferSubData(this.gl.ELEMENT_ARRAY_BUFFER, offset, data);
        return this;
    }
};
