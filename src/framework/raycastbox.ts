import RayCaster from "../framework/raycast";
import Ray from "../framework/ray";
import Mesh from "../framework/mesh";
import AABB from "../math/aabb";
import {PerspectiveCamera} from "../framework/camera";

/**
 * Helper class to check against a mesh's bounding box
 */
export default class RayCastBox {

    raycaster: RayCaster = new RayCaster([0, 0, 0], [0, 0, 0])

    ray: Ray = new Ray()

    box: AABB = new AABB()

    /**
     * Updates the [Raycaster] object
     * @param origin {Vec3} a 3 item array/TypedArray
     * @param direction {Vec3} a 3 item array/TypedArray
     */
    setCaster(origin: Vec3, direction: Vec3) {
        this.raycaster.set(origin, direction)
    }

    /**
     * Updates the ray for checking against a bounding box
     * @param coords {Object} an object containing x and y keys indicating the mouse position
     * @param camera {PerspectiveCamera} the perspective camera to check against.
     */
    update(coords: any, camera: PerspectiveCamera) {
        this.raycaster.setFromPerspectiveCamera(coords, camera)

        this.ray.copy(this.raycaster.ray).recast(this.raycaster.near)

        // need to invert - for some reason, glMatrix makes things negative when using lookAt. If not negative then
        // pass through
        this.ray.origin[2] = this.ray.origin[2] < 0 ? this.ray.origin[2] *= -1 : this.ray.origin[2]
    }

    checkMesh(mesh: Mesh) {
        this.box.setFromAttribute(mesh.getAttribute("position"))
        return this.ray.intersectBox(this.box)
    }

}
