import * as mat4 from "../math/mat4";
import * as vec3 from "../math/vec3";
import vao from "../core/vao"
import vbo from "../core/vbo"
import {getBytesPerValue} from "../utils";
import Sphere from "../math/sphere";
import {Camera, Geometry} from "../core/interfaces";

// TODO add typing around attributes
type AttribData = {
    data: Float32Array,
    size: number
    name: string
    normalized: boolean
    stride: number
}

/**
 * This represents something to be rendered on screen.
 */
export default class Mesh {
    gl: any;
    vao: any;
    attributes: any;
    attribData: Array<any>;
    shader: any;
    primMode: number;

    // when instancing, this describes the number of items to draw.
    numItems: number;

    // if using a framebuffer, stores framebuffer.
    framebuffer: any = null;

    // number of vertices to render
    numVertices: number;

    // whether or not the mesh is an indexed mesh.
    isIndexed: boolean = false;

    // a custom index vbo to use
    indexVbo: any;

    // typed array of index data
    indexData: ArrayBufferView

    // whether or not the mesh is an instanced mesh.
    isInstanced: boolean = false;

    // the model matrix for the mesh.
    modelMatrix: Float32Array | any[] = mat4.create();

    // current position of the object.
    position: Float32Array | any[] = vec3.create();

    boundingSphere: Sphere = null

    constructor(gl: WebGL2RenderingContext, shader: WebGLProgram, geometry?: Geometry) {
        this.gl = gl;
        this.shader = shader;
        this.vao = vao(gl);
        this.attributes = []
        this.attribData = [];
        this.numVertices = 0;
        this.primMode = gl.TRIANGLES;

        if (geometry !== undefined) {
            this._processGeometry(geometry)
        }
    }

    setPrimitiveMode(mode: number) {
        this.primMode = mode;
        return this;
    }

    setShader(shader: WebGLProgram) {
        this.shader = shader
    }

    /**
     * Translate the mesh. Does not translate the mesh in an additive manner, but simply changes the
     * necessary blocks to their new value.
     * @param pos {Array} a 3 component array representing a translation vector
     */
    translate(pos: number[]) {

        this.position = pos !== undefined ? pos : this.position;

        this.modelMatrix[12] = this.position[0];
        this.modelMatrix[13] = this.position[1];
        this.modelMatrix[14] = this.position[2];
        return this;
    }

    getPosition() {
        return this.position;
    }


    getX() {
        return this.modelMatrix[12];
    }

    getY() {
        return this.modelMatrix[13];
    }

    getZ() {
        return this.modelMatrix[14];
    }

    /**
     * Translates the model matrix in an additive manner.
     * @param pos {Array} a 3 component array representing a translation vector
     */
    additiveTranslate(pos: number[]) {
        mat4.translate(this.modelMatrix, this.modelMatrix, pos);
        return this;
    }

    drawWithShader(shader, camera = null, cb: Function = null) {
        let gl = this.gl;


        // if we're drawing into a framebuffer, bind it.
        if (this.framebuffer) {
            this.framebuffer.bind();
        }

        // bind shader
        shader.bind();


        // bind vao
        this.vao.bind();

        // if camera was used
        if (camera !== null) {
            shader.uniform("projectionMatrix", camera.projectionMatrix);
            shader.uniform("viewMatrix", camera.viewMatrix);

            shader.uniform("modelMatrix", this.modelMatrix);
        }

        // send the resolution by default
        shader.uniform("resolution", [this.gl.canvas.width, this.gl.canvas.height]);

        // send app runtime by default.
        shader.float("time", performance.now() * 0.0005);


        // if user wants to override default uniforms or set additional uniforms,
        // trigger convenience callback here.
        if (cb) {
            cb(shader);
        }

        // enable mesh attributes.
        this._enableAttributes();


        // if mesh is instanced
        if (this.isInstanced) {


            // if mesh is indexed
            if (this.isIndexed) {

                this.indexVbo.bind();
                gl.drawElementsInstanced(this.primMode, this.numVertices, gl.UNSIGNED_SHORT, 0, this.numItems);
                this.indexVbo.unbind();
            } else {

                gl.drawArraysInstanced(this.primMode, 0, this.numVertices, this.numItems);
            }

        } else {

            // if mesh is indexed
            if (this.isIndexed) {

                this.indexVbo.bind();
                gl.drawElements(this.primMode, this.numVertices, gl.UNSIGNED_SHORT, 0);
                this.indexVbo.unbind();
            } else {

                gl.drawArrays(this.primMode, 0, this.numVertices);
            }
        }

        // disable currently active attribute
        this._disableAttributes();

        this.vao.unbind();

        if (this.framebuffer) {
            this.framebuffer.unbind();
        }

    }


    /**
     * Draws the mesh
     * @param camera {Camera} an object that contains the keys "projectionMatrix" and "viewMatrix"
     * @param cb {Function} a function to run to make it simpler to set shader uniforms on a mesh. Will receive shader
     *     object
     */
    draw(
        {
            camera = null,
            cb = null
        }: {
            camera?: Camera,
            cb?: Function
        } = {}) {

        let gl = this.gl;

        // bind shader
        this.shader.bind();

        // bind vao
        this.vao.bind();

        // if camera was used
        if (camera !== null) {
            this.shader.uniform("projectionMatrix", camera.projectionMatrix);
            this.shader.uniform("viewMatrix", camera.viewMatrix);
            this.shader.uniform("modelMatrix", this.modelMatrix);
        }


        // send the resolution by default
        this.shader.uniform("resolution", [this.gl.canvas.width, this.gl.canvas.height]);

        // send app runtime by default.
        this.shader.float("time", performance.now() * 0.0005);


        // if user wants to override default uniforms or set additional uniforms,
        // trigger convenience callback here.
        if (cb) {
            cb(this.shader);
        }

        // enable mesh attributes.
        this._enableAttributes();

        // if mesh is instanced
        if (this.isInstanced) {
            // if mesh is indexed
            if (this.isIndexed) {

                this.indexVbo.bind();
                gl.drawElementsInstanced(this.primMode, this.numVertices, gl.UNSIGNED_SHORT, 0, this.numItems);
                this.indexVbo.unbind();
            } else {

                gl.drawArraysInstanced(this.primMode, 0, this.numVertices, this.numItems);
            }

        } else {

            // if mesh is indexed
            if (this.isIndexed) {

                this.indexVbo.bind();
                gl.drawElements(this.primMode, this.numVertices, gl.UNSIGNED_SHORT, 0);
                this.indexVbo.unbind();
            } else {

                gl.drawArrays(this.primMode, 0, this.numVertices);
            }
        }

        // disable currently active attribute
        this._disableAttributes();

        this.vao.unbind();
    }

    /**
     * Sets a uniform value on the mesh. Note that this runs the general shader uniform function so it may or may not
     * work depending on the value that you would like to send.
     * @param name {string} the name of the uniform.
     * @param value {*} the value for the uniform
     */
    uniform(name, value) {
        this.shader.uniform(name, value);
        return this;
    }

    /**
     * Processes a geometry object.
     * @param geo {Geometry} a Geometry object
     * @private
     */
    _processGeometry(geo: Geometry) {

        // add positions because we at least know that should exist.
        this.addAttribute('position', new Float32Array(geo.vertices), {
            size: geo.customSize
        });

        // add uvs
        if (geo.uvs.length > 0) {
            this.addAttribute('uv', new Float32Array(geo.uvs), {
                size: 2
            });
        }

        // if normals exist add normals
        if (geo.normals.length > 0) {
            this.addAttribute('normal', new Float32Array(geo.normals), {
                size: 3
            });
        }

        // add indices
        if (geo.indices.length > 0) {
            this.addIndexBuffer(new Uint16Array(geo.indices));
        }

    }

    /**
     * Enables all attributes for the mesh.
     * @private
     */
    _enableAttributes() {
        for (let i in this.attribData) {
            let attrib = this.attribData[i];

            this.vao.enable(attrib.location);
        }
    }

    /**
     * Disables all attributes for the mesh.
     * @private
     */
    _disableAttributes() {
        for (let i in this.attribData) {
            let attrib = this.attribData[i];
            this.vao.disable(attrib.location);
        }
    }

    /**
     * Scales model matrix in a non-cumulative manner, essentially replacing the necessary values.
     * @param scale
     */
    scale(scale: number[]) {
        this.modelMatrix[0] = scale[0]
        this.modelMatrix[5] = scale[1];
        this.modelMatrix[10] = scale[2];
        return this;
    }

    /**
     * Scales model matrix in a cumulative manner
     * @param scale
     */
    scaleCumulative(scale: number[]) {
        this.modelMatrix = mat4.scale(this.modelMatrix, this.modelMatrix, scale);

        return this;
    }

    /**
     * Sets an fbo for the mesh  - if set, mesh will draw onto FBO instead of
     * rendering to screen.
     * @param fbo
     */
    setFbo(fbo: WebGLFramebuffer) {
        this.framebuffer = fbo;
        return this;
    }

    /**
     * When rendering onto a framebuffer, this will return the Texture associated with
     * COLOR_ATTACHMENT0.
     *
     * If not rendering with a framebuffer, will just return the mesh object
     */
    getRenderedMesh() {
        if (this.framebuffer) {
            return this.framebuffer.getColorTexture();
        } else {
            console.warn("Mesh::getRenderedMesh called with no FBO assigned. Returning mesh object");
            return this;
        }
    }

    /**
     * Adds an indices to a mesh
     * @param data {Array} array of indices to utilize.
     */
    addIndexBuffer(data: ArrayBufferView) {
        let gl = this.gl;
        this.vao.bind();

        this.indexData = data

        this.indexVbo = vbo(gl, {
            indexed: true,
            data
        });

        this.vao.unbind();
        this.isIndexed = true;
        this.numVertices = data.buffer.byteLength / getBytesPerValue(data)
        return this;
    }

    /**
     * Adds an instanced attribute
     * @param name {string} name for the attribute
     * @param data {Array} data to populate attribute buffer
     * @param size {number} the size of each component of the attribute ie 3d position would be 3
     * @param divisor {number} number of instances that will get run during update. Usually 1 is fine
     * @param normalized {number} whether or not the data is normalized.
     * @param format {number} GLenum of what type of data the attribute contains - ie gl.FLOAT
     * @param stride
     * @param offset
     */
    addInstancedAttribute(name: string, data: ArrayBufferView | WebGLBuffer, {
        size = 3,
        divisor = 1,
        normalized = this.gl.FALSE,
        format = this.gl.FLOAT,
        stride = 0,
        offset = 0
    } = {}) {

        this.addAttribute(name, data, {
            size: size,
            normalized: normalized,
            format: format,
            stride: stride,
            offset: offset
        });

        let attrib = this.attribData.filter(attrib => {
            if (name === attrib.name) {
                return attrib;
            }
        })[0]

        if (attrib === null || attrib === undefined) {
            console.log("unable to find attribute", name, " Check addAttribute function in DisplayObject to make sure it was added correctly.");
            return;
        }

        this.vao.bind();

        attrib.buffer.bind();
        this.vao.makeInstancedAttribute(attrib.location, divisor);
        attrib.buffer.unbind();
        this.vao.unbind();

        this.isInstanced = true;

    }

    /**
     * Adds an attribute to the mesh via a buffer instead of an array.
     * @param name {string} name of the attribute in the shader
     * @param buffer {WebGLBuffer} a WebGL buffer of pre-loaded data.
     * @param size {number} the size of each component per vertex
     * @param normalized {boolean} whether or not the data is normalized
     * @param format {number} GLenum indicating the type of information for the attribute.
     * @param stride {number}
     * @param offset {number}
     */
    addAttributeBuffer(name: string, buffer: any, {
        size = 3,
        normalized = this.gl.FALSE,
        format = this.gl.FLOAT,
        stride = 0,
        offset = 0
    } = {}) {

        this.vao.bind();
        buffer.bind();

        // check to see if we already have an attribute for the data
        if (this.attributes.hasOwnProperty(name)) {
            let attrib = this.attributes[name];
            this.vao.enable(attrib.location);
            this.vao.assignData(attrib.location, size, format, normalized, stride, offset);
            attrib["data"] = null;
            attrib["buffer"] = buffer;
            this.vao.disable(attrib.location);
            this.attribData.push(attrib);
        } else {

            // create a new attribute and assign it a location
            let loc = this.attribData.length;
            let attrib = {
                location: loc,
                name: name,
                data: null,
                buffer: buffer
            }
            this.gl.bindAttribLocation(this.shader, attrib.location, name);
            this.vao.enable(loc);
            this.vao.assignData(loc, size, format, normalized, stride, offset);
            this.vao.disable(attrib.location);
            this.attribData.push(attrib);
        }
        buffer.unbind();
        this.vao.unbind();

    }

    /**
     * Adds an instanced attribute to the mesh via a buffer instead of an array.
     * @param name {string} name of the attribute in the shader
     * @param buffer {WebGLBuffer} a WebGL buffer of pre-loaded data.
     * @param size {number} the size of each component per vertex
     * @param normalized {boolean} whether or not the data is normalized
     * @param format {number} GLenum indicating the type of information for the attribute.
     * @param stride {number}
     * @param offset {number}
     * @param divisor {number}
     * @param numItems {number}
     */
    addInstancedAttributeBuffer(name: string, buffer: any, {
        size = 3,
        normalized = this.gl.FALSE,
        format = this.gl.FLOAT,
        stride = 0,
        offset = 0,
        divisor = 1,
        numItems = 100
    }: {
        size?: number,
        normalized?: GLenum | boolean,
        format?: GLenum,
        stride?: number,
        offset?: number,
        divisor?: number,
        numItems?: number

    } = {}) {

        this.vao.bind();
        buffer.bind();

        // check to see if we already have an attribute for the data
        if (this.attributes.hasOwnProperty(name)) {
            let attrib = this.attributes[name];
            this.vao.enable(attrib.location);
            this.vao.assignData(attrib.location, size, format, normalized, stride, offset);
            attrib["data"] = null;
            attrib["buffer"] = buffer;
            this.vao.makeInstancedAttribute(attrib.location, divisor);
            this.vao.disable(attrib.location);
            this.attribData.push(attrib);
        } else {
            // create a new attribute and assign it a location
            let loc = this.attribData.length;
            let attrib = {
                location: loc,
                name: name,
                data: null,
                buffer: buffer
            }
            this.gl.bindAttribLocation(this.shader, attrib.location, name);
            this.vao.enable(loc);
            this.vao.assignData(loc, size, format, normalized, stride, offset);
            this.vao.makeInstancedAttribute(loc, divisor);
            this.vao.disable(attrib.location);
            this.attribData.push(attrib);
        }
        buffer.unbind();
        this.vao.unbind();


        this.isInstanced = true;
        this.numItems = numItems;
        return this;

    }

    /**
     * Updates the specified attribute
     * @param name {string} name of the attribute.
     * @param data {Array} array of data. can also be typed array
     */
    updateAttribute(name: string, data: ArrayBufferView) {
        let attrib = this.attribData.filter(_attrib => {
            if (_attrib.name === name) {
                return _attrib;
            }
        })[0]

        if (attrib === undefined) {
            console.log("Can't update attribute with the name of ", name, "; does it exist? ");
            return;
        }

        // save updated data in the attribute and update
        attrib.data = data
        attrib.buffer.updateBuffer(data, 0)
    }

    /**
     * Add an attribute to the mesh
     * @param name {string} name of the attribute in the shader
     * @param data {Array} data array of attribute info
     * @param size {number} the size of each component per vertex
     * @param normalized {boolean} whether or not the data is normalized
     * @param format {number} GLenum indicating the type of information for the attribute.
     * @param stride
     * @param offset
     */
    addAttribute(name: string, data: ArrayBufferView | WebGLBuffer, {
        size = 3,
        normalized = this.gl.FALSE,
        format = this.gl.FLOAT,
        stride = 0,
        offset = 0
    } = {}) {

        let arr = data

        // bind vao and build buffer for attribute
        this.vao.bind();
        let buffer = vbo(this.gl, {
            indexed: false,
            data: arr
        });

        buffer.bind();

        // check to see if we already have an attribute for the data, if we do then update.
        if (this.attributes.hasOwnProperty(name)) {

            let attrib = this.attributes[name];
            this.vao.enable(attrib.location);
            this.vao.assignData(attrib.location, size, format, normalized, stride, offset);
            attrib["data"] = arr;
            attrib["size"] = size
            attrib["buffer"] = buffer;
            this.vao.disable(attrib.location);
            this.attribData.push(attrib);

        } else {

            // create a new attribute and assign it a location
            let loc = this.attribData.length
            let attrib = {
                location: loc,
                name: name,
                data: arr,
                size: size,
                buffer: buffer
            }

            this.gl.bindAttribLocation(this.shader, attrib.location, name);
            this.vao.enableAttrib(loc);
            this.vao.assignData(loc, size, format, normalized, stride, offset);
            this.vao.disableAttrib(attrib.location);
            this.attribData.push(attrib);
        }

        // figure out the number of vertices to use when drawing.
        if (name === "position" || name === "Position") {
            if ("buffer" in data) {
                this.numVertices = (data.buffer.byteLength / getBytesPerValue(data)) / size
            }
        }

        buffer.unbind();
        this.vao.unbind();

        return this;
    }

    debugClearAttributes() {
        this.attributes = [];
        this.attribData = [];
    }

    /**
     * Rotates the model matrix
     * @param angle {number} the angle to rotate by
     * @param axis {Array} a 3 item array specifying the axis to rotate on
     */
    rotate(angle: number, axis: number[]) {
        mat4.rotate(this.modelMatrix, this.modelMatrix, angle, axis);

        return this;
    }

    /**
     * Sets the numItems property of the mesh. Used when drawings an instance mesh.
     * @param itms{number} the number of instances to draw
     */
    setNumItems(itms: number = 1) {
        this.numItems = itms;
        return this;
    }

    hasAttrib(name: string) {
        let data = this.attribData.filter(attrib => attrib.name === name)
        return data.length > 0
    }

    // based on
    //https://github.com/oframe/ogl/blob/eab9e72931cab5b53badf6c872216f6424fbbd25/src/core/Geometry.js#L210
    computeBoundingBox() {

    }

    getAttribute(name: string) {
        let data = this.attribData.filter(attrib => attrib.name === name)
        if (data.length > 0) {
            return data[0]
        }

        console.error("No attribute with the name ", name)
    }

    /**
     * Allows you to set the number of vertices manually.
     */
    setNumVertices(num: number) {

        this.numVertices = num;
        return this;
    }

    // returns data associated with an attribute.
    getAttributeData(name: string) {

        return this.attributes[name];
    }

    computeBoundingSphere() {

        if (!this.boundingSphere) {
            this.boundingSphere = new Sphere()
        }

        if (this.hasAttrib("position")) {
            const center = this.boundingSphere.center

        }
    }
}
