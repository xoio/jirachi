
export default class FBOPicker {

    pickPixelSize = 6

    gl: WebGL2RenderingContext

    constructor(ctx: WebGL2RenderingContext) {

        this.gl = ctx;
    }


    /**
     * Attempts to start picking in the FBO.
     * @param fbo {WebGLFramebuffer} the framebuffer to read from
     * @param mouse {MousePos} the mouse position to use
     * @param w {number} the width of the framebuffer
     * @param h {number} the height of the framebuffer.
     */
    pick(fbo: WebGLFramebuffer, mouse: any, w: number, h: number) {
        //Surface8u pixels = mFbo->readPixels8u( Area( mousePos - ivec2( mPickPixelSize / 2 ), mousePos + ivec2( mPickPixelSize / 2 ) ), GL_COLOR_ATTACHMENT1 );

        let pickSize = [this.pickPixelSize / 2, this.pickPixelSize / 2]

        let x = mouse.x - pickSize[0]
        let y = mouse.y - pickSize[1];

        let width = mouse.x + pickSize[0]
        let height = mouse.y + pickSize[1]

        let pixels = new Uint8Array(width * height);
        this.gl.readPixels(x, y, width, height, this.gl.UNSIGNED_BYTE, this.gl.RGBA, pixels)

    }


}