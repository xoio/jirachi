import {Geometry} from "../core/interfaces";
import createCube from "primitive-cube"

export class Quad implements Geometry {
    vertices: Array<any> = [-1, -1, -1, 4, 4, -1];
    normals: Array<any> = [];
    indices: Array<any> = [];
    uvs: Array<any> = [];
    customSize: number = 2;

    constructor() {
    }
}

/**
 * Based off of @vorg's primitive-cube package.
 * Mostly the same but with some minor modifications.
 * https://github.com/vorg/primitive-cube
 */
export class Cube implements Geometry {
    customSize: number;
    indices: Array<any> = []
    normals: Array<any> = []
    uvs: Array<any> = []
    vertices: Array<number> = []

    constructor(w: number, h: number, d: number, wSeg: number, hSeg: number, dSeg: number) {

        // build cube and unroll values.
        let cube = createCube(w, h, d, wSeg, hSeg, dSeg)
        cube.positions.forEach(p => {
            this.vertices.push(p[0], p[1], p[2])
        })

        cube.cells.forEach(p => {
            this.indices.push(p[0], p[1], p[2])
        })

        cube.normals.forEach(p => {
            this.normals.push(p[0], p[1], p[2])
        })

        cube.uvs.forEach(p => {
            this.uvs.push(p[0], p[1])
        })
    }

}

/**
 * Based off of @vorg's primitive-plane package.
 * Mostly the same but with some minor modifications.
 * https://github.com/vorg/primitive-plane
 */
export class Plane implements Geometry {
    vertices: Array<any> = [];
    normals: Array<any> = [];
    indices: Array<any> = [];
    uvs: Array<any> = [];
    customSize: number = 3;

    constructor(sx = 1, sy = 1, nx = 1, ny = 1, options = {
        topLeft: false
    }) {
        let quads = (options && options["quads"]) ? options["quads"] : false
        let resolution = (options && options["resolution"]) ? options["resolution"] : 1


        for (let iy = 0; iy <= ny; iy++) {
            for (let ix = 0; ix <= nx; ix++) {
                let u = ix / nx
                let v = iy / ny

                let x = -sx / 2 + u * sx // starts on the left
                let y = sy / 2 - v * sy // starts at the top


                let z = (ix * resolution);

                if (options.topLeft) {
                    x += (sx / 2)
                    y += (sy / 2)
                }

                this.vertices.push(x, y, 0)
                this.uvs.push(u, 1.0 - v)
                this.normals.push(0, 0, 1)

                if (iy < ny && ix < nx) {
                    if (quads) {
                        this.indices.push([iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix + 1])
                    } else {
                        this.indices.push([iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix + 1])
                        this.indices.push([(iy + 1) * (nx + 1) + ix + 1, iy * (nx + 1) + ix, (iy + 1) * (nx + 1) + ix])
                    }
                }
            }
        }

        // unroll indices
        let idx = [];
        this.indices.forEach(set => {
            set.forEach(itm => {
                idx.push(itm);
            })
        })

        this.indices = idx;
    }
}
