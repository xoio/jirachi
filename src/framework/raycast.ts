import Ray from "../framework/ray";
import {create, normalize, setFromMatrixPosition, unProject} from "../math/vec3";
import {PerspectiveCamera} from "../framework/camera";

export default class Raycaster {

    near: number = 0
    far: number = 0
    ray: Ray
    camera: PerspectiveCamera

    constructor(origin: Vec3, direction: Vec3, near = 0, far = Infinity) {

        this.ray = new Ray(origin, direction)
        this.near = near
        this.far = far

    }

    set(origin: Vec3, direction: Vec3) {
        this.ray.set(origin, direction)
    }


    // TODO only supporting perspective cameras for the time being
    setFromPerspectiveCamera(coords: Vec3, camera: PerspectiveCamera) {

        // not using matrix world
        setFromMatrixPosition(this.ray.origin, camera.viewMatrix)

        this.ray.direction[0] = coords[0]
        this.ray.direction[1] = coords[1]
        this.ray.direction[2] = (camera.near + camera.far) / (camera.near - camera.far)

        unProject(this.ray.direction, camera.getInverseProjection(), camera.viewMatrix)

        this.ray.direction[0] -= this.ray.origin[0]
        this.ray.direction[1] -= this.ray.origin[1]
        this.ray.direction[2] -= this.ray.origin[2]

        normalize(this.ray.direction, this.ray.direction)
        this.camera = camera

    }

    intersectObjects() {

    }

}
