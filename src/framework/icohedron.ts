import {Geometry} from "../core/interfaces";
import * as vec3 from "../math/vec3";

// more or less a port of https://github.com/hughsk/icosphere mashed together with
// https://github.com/glo-js/primitive-icosphere
// but adapted so as not to be reliant on a commonjs module
export default class Icohedron implements Geometry {

    customSize: number
    indices: Array<any> = []
    normals: Array<any> = []
    uvs: Array<any> = []
    vertices: Array<number> = []

    constructor(subdivisions: number = 3) {
        this._buildMesh(subdivisions)
    }

    _buildMesh(subdivisions) {

        let positions = []
        let faces = []
        let ids = [];
        let t = 0.5 + Math.sqrt(5) / 2

        positions.push([-1, +t, 0])
        positions.push([+1, +t, 0])
        positions.push([-1, -t, 0])
        positions.push([+1, -t, 0])

        positions.push([0, -1, +t])
        positions.push([0, +1, +t])
        positions.push([0, -1, -t])
        positions.push([0, +1, -t])

        positions.push([+t, 0, -1])
        positions.push([+t, 0, +1])
        positions.push([-t, 0, -1])
        positions.push([-t, 0, +1])


        faces.push([0, 11, 5])
        faces.push([0, 5, 1])
        faces.push([0, 1, 7])
        faces.push([0, 7, 10])
        faces.push([0, 10, 11])

        faces.push([1, 5, 9])
        faces.push([5, 11, 4])
        faces.push([11, 10, 2])
        faces.push([10, 7, 6])
        faces.push([7, 1, 8])

        faces.push([3, 9, 4])
        faces.push([3, 4, 2])
        faces.push([3, 2, 6])
        faces.push([3, 6, 8])
        faces.push([3, 8, 9])

        faces.push([4, 9, 5])
        faces.push([2, 4, 11])
        faces.push([6, 2, 10])
        faces.push([8, 6, 7])
        faces.push([9, 8, 1])


        let complex = {
            cells: faces
            , positions: positions
        }

        while (subdivisions-- > 0) {

            complex = this._subdivide(complex)
        }

        positions = complex.positions

        for (let i = 0; i < positions.length; i++) {

            this._normalize(positions[i])
        }

        for (let i = 0; i < complex.positions.length; ++i) {
            ids.push(Math.random());
        }
        // ============ BUILD UVs and NORMALS ====================== //
        let uvs = [];
        let normals = [];

        for (let i = 0; i < complex.positions.length; ++i) {
            let position = complex.positions[i];
            let u = 0.5 * (-(Math.atan2(position[2], -position[0]) / Math.PI) + 1.0)
            let v = 0.5 + Math.asin(position[1]) / Math.PI
            uvs.push(1 - u, 1 - v)

            let normal = vec3.normalize([0, 0, 0], position);
            normals.push(normal);
        }

        this.vertices = positions.flat()
        this.normals = normals.flat()
        this.uvs = uvs.flat()
        this.indices = complex.cells.flat()
    }

    _normalize(vec) {
        let mag = 0
        for (let n = 0; n < vec.length; n++) {
            mag += vec[n] * vec[n]
        }
        mag = Math.sqrt(mag)

        // avoid dividing by zero
        if (mag === 0) {
            return Array.apply(null, new Array(vec.length)).map(Number.prototype.valueOf, 0)
        }

        for (let n = 0; n < vec.length; n++) {
            vec[n] /= mag
        }

        return vec
    }

    _subdivide(complex) {
        let positions = complex.positions
        let cells = complex.cells

        let newCells = []
        let newPositions = []
        let midpoints = {}
        let f = [0, 1, 2]
        let l = 0

        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i]
            let c0 = cell[0]
            let c1 = cell[1]
            let c2 = cell[2]
            let v0 = positions[c0]
            let v1 = positions[c1]
            let v2 = positions[c2]

            let a = getMidpoint(v0, v1)
            let b = getMidpoint(v1, v2)
            let c = getMidpoint(v2, v0)

            let ai = newPositions.indexOf(a)
            if (ai === -1) ai = l++, newPositions.push(a)
            let bi = newPositions.indexOf(b)
            if (bi === -1) bi = l++, newPositions.push(b)
            let ci = newPositions.indexOf(c)
            if (ci === -1) ci = l++, newPositions.push(c)

            let v0i = newPositions.indexOf(v0)
            if (v0i === -1) v0i = l++, newPositions.push(v0)
            let v1i = newPositions.indexOf(v1)
            if (v1i === -1) v1i = l++, newPositions.push(v1)
            let v2i = newPositions.indexOf(v2)
            if (v2i === -1) v2i = l++, newPositions.push(v2)

            newCells.push([v0i, ai, ci])
            newCells.push([v1i, bi, ai])
            newCells.push([v2i, ci, bi])
            newCells.push([ai, bi, ci])
        }

        return {
            cells: newCells,
            positions: newPositions
        }

        // reuse midpoint vertices between iterations.
        // Otherwise, there'll be duplicate vertices in the final
        // mesh, resulting in sharp edges.
        function getMidpoint(a, b) {
            let point = midpoint(a, b)
            let pointKey = pointToKey(point)
            let cachedPoint = midpoints[pointKey]
            if (cachedPoint) {
                return cachedPoint
            } else {
                return midpoints[pointKey] = point
            }
        }

        function pointToKey(point) {
            return point[0].toPrecision(6) + ','
                + point[1].toPrecision(6) + ','
                + point[2].toPrecision(6)
        }

        function midpoint(a, b) {
            return [
                (a[0] + b[0]) / 2
                , (a[1] + b[1]) / 2
                , (a[2] + b[2]) / 2
            ]
        }
    }

}
