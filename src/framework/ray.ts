import {
    create,
    normalize,
    subtract,
    dot,
    squaredLength,
    squaredDistance,
    distance,
    add,
    scale,
    transformMat4, transformDirection
} from "../math/vec3"
import Sphere from "../math/sphere";
import AABB from "../math/aabb";


/**
 * Based off of the Three.js Ray class
 * https://github.com/mrdoob/three.js/blob/master/src/math/Ray.js
 */

let _vector = create() as Vec3
let _segCenter = create() as Vec3
let _segDir = create() as Vec3
let _diff = create() as Vec3

let _edge1 = create() as Vec3
let _edge2 = create() as Vec3
let _normal = create() as Vec3

export default class Ray {

    origin: Vec3
    direction: Vec3

    constructor(
        origin: Vec3 = new Float32Array([0, 0, 0]),
        direction: Vec3 = new Float32Array([0, 0, 0]),
    ) {

        this.origin = origin
        this.direction = direction

    }

    set(origin: Vec3, direction: Vec3) {
        this.origin = new Float32Array(origin)
        this.direction = new Float32Array(direction)
        return this
    }

    at(t: number, target: Vec3) {

        target = new Float32Array(this.origin)
        target[0] += this.direction[0] * t
        target[1] += this.direction[1] * t
        target[2] += this.direction[2] * t
        return target
    }

    lookAt(v: Float32Array) {
        this.direction = new Float32Array(v)
        this.direction[0] -= this.origin[0]
        this.direction[1] -= this.origin[1]
        this.direction[2] -= this.origin[2]

        normalize(this.direction, this.direction)
    }

    copy(ray: Ray) {
        this.origin[0] = ray.origin[0]
        this.origin[1] = ray.origin[1]
        this.origin[2] = ray.origin[2]

        this.direction[0] = ray.direction[0]
        this.direction[1] = ray.direction[1]
        this.direction[2] = ray.direction[2]
        return this
    }

    recast(t: number) {
        this.origin = new Float32Array(this.at(t, _vector))
        return this
    }

    closestPointToPoint(point: Float32Array, target: Float32Array) {

        subtract(target, point, this.origin)
        const directionDistance = dot(target, this.direction)
        if (directionDistance < 0) {
            return new Float32Array(this.origin)
        }

        target = new Float32Array(this.origin)
        target[0] += this.direction[0] * directionDistance
        target[1] += this.direction[1] * directionDistance
        target[2] += this.direction[2] * directionDistance

        return target
    }

    distanceToPoint(point: Vec3) {
        return Math.sqrt(this.distanceSqToPoint(point))
    }

    distanceSqToPoint(point: Vec3) {

        subtract(_vector, point, this.origin)

        const directionDistance = dot(
            _vector,
            this.direction
        )


        if (directionDistance < 0) {
            return squaredDistance(this.origin, point)
        }

        _vector = new Float32Array(this.origin)
        _vector[0] += this.direction[0] * directionDistance;
        _vector[1] += this.direction[1] * directionDistance;
        _vector[2] += this.direction[2] * directionDistance;

        return squaredDistance(_vector, point)
    }

    _distanceToSquared(i: Vec3, o: Vec3) {
        const dx = i[0] - o[0], dy = i[1] - o[1], dz = i[2] - o[2];
        return dx * dx + dy * dy + dz * dz;
    }

    distanceSqSegment(v0: Float32Array, v1: Float32Array, {
        optionalPointOnRay = null,
        optionalPointOnSegment = null
    } = {}) {

        _segCenter = new Float32Array(v0)
        add(_segCenter, _segCenter, v1)
        scale(_segCenter, _segCenter, 0.5)

        _segDir = new Float32Array(v1)
        subtract(_segDir, _segDir, v0)
        normalize(_segDir, _segDir)

        _diff = new Float32Array(this.origin)
        subtract(_diff, _diff, _segCenter)

        const segExtent = distance(v0, v1) * 0.5
        const a01 = -dot(this.direction, _segDir)
        const b0 = dot(_diff, this.direction);
        const b1 = -dot(_diff, _segDir);
        const c = squaredLength(_diff)
        const det = Math.abs(1 - a01 * a01);
        let s0, s1, sqrDist, extDet;


        if (det > 0) {

            // The ray and segment are not parallel.

            s0 = a01 * b1 - b0;
            s1 = a01 * b0 - b1;
            extDet = segExtent * det;

            if (s0 >= 0) {

                if (s1 >= -extDet) {

                    if (s1 <= extDet) {

                        // region 0
                        // Minimum at interior points of ray and segment.

                        const invDet = 1 / det;
                        s0 *= invDet;
                        s1 *= invDet;
                        sqrDist = s0 * (s0 + a01 * s1 + 2 * b0) + s1 * (a01 * s0 + s1 + 2 * b1) + c;

                    } else {

                        // region 1

                        s1 = segExtent;
                        s0 = Math.max(0, -(a01 * s1 + b0));
                        sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;

                    }

                } else {

                    // region 5

                    s1 = -segExtent;
                    s0 = Math.max(0, -(a01 * s1 + b0));
                    sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;

                }

            } else {

                if (s1 <= -extDet) {

                    // region 4

                    s0 = Math.max(0, -(-a01 * segExtent + b0));
                    s1 = (s0 > 0) ? -segExtent : Math.min(Math.max(-segExtent, -b1), segExtent);
                    sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;

                } else if (s1 <= extDet) {

                    // region 3

                    s0 = 0;
                    s1 = Math.min(Math.max(-segExtent, -b1), segExtent);
                    sqrDist = s1 * (s1 + 2 * b1) + c;

                } else {

                    // region 2

                    s0 = Math.max(0, -(a01 * segExtent + b0));
                    s1 = (s0 > 0) ? segExtent : Math.min(Math.max(-segExtent, -b1), segExtent);
                    sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;

                }

            }

        } else {

            // Ray and segment are parallel.

            s1 = (a01 > 0) ? -segExtent : segExtent;
            s0 = Math.max(0, -(a01 * s1 + b0));
            sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;

        }

        if (optionalPointOnRay) {
            optionalPointOnRay = new Float32Array(this.origin)
            optionalPointOnRay[0] += this.direction[0] * s0;
            optionalPointOnRay[1] += this.direction[1] * s0;
            optionalPointOnRay[2] += this.direction[2] * s0;
        }

        if (optionalPointOnSegment) {
            optionalPointOnSegment = new Float32Array(_segCenter)
            optionalPointOnSegment[0] += _segDir[0] * s1
            optionalPointOnSegment[1] += _segDir[1] * s1
            optionalPointOnSegment[2] += _segDir[2] * s1
        }
    }

    applyMat4(mat: Mat4) {
        transformMat4(this.origin, this.origin, mat)
        transformDirection(this.direction, mat)
    }

    intersectsSphere(sphere) {

        return this.distanceSqToPoint(sphere.center) <= (sphere.radius * sphere.radius);

    }

    intersectSphere(sphere: Sphere, target: Vec3) {
        _vector[0] = sphere.center[0] - this.origin[0]
        _vector[1] = sphere.center[1] - this.origin[1]
        _vector[2] = sphere.center[2] - this.origin[2]

        const tca = dot(_vector, this.direction)
        const d2 = dot(_vector, _vector) - tca * tca;
        const radius2 = sphere.radius * sphere.radius

        if (d2 > radius2) {
            return null
        }

        const thc = Math.sqrt(radius2 - d2)

        // t0 = first intersect point - entrance on front of sphere
        const t0 = tca - thc;

        // t1 = second intersect point - exit point on back of sphere
        const t1 = tca + thc;

        // test to see if t1 is behind the ray - if so, return null
        if (t1 < 0) return null;

        // test to see if t0 is behind the ray:
        // if it is, the ray is inside the sphere, so return the second exit point scaled by t1,
        // in order to always return an intersect point that is in front of the ray.
        if (t0 < 0) return this.at(t1, target);

        // else t0 is in front of the ray, so return the first collision point scaled by t0
        return this.at(t0, target);
    }

    intersectBox(box: AABB) {
        return this.intersectsBox(box, _vector) !== null;
    }

    intersectsBox(box: AABB, target: Vec3) {
        let tmin, tmax, tymin, tymax, tzmin, tzmax;

        const invdirx = 1 / this.direction[0],
            invdiry = 1 / this.direction[1],
            invdirz = 1 / this.direction[2];


        const origin = this.origin;

        if (invdirx >= 0) {

            tmin = (box.min[0] - origin[0]) * invdirx;
            tmax = (box.max[0] - origin[0]) * invdirx;

        } else {

            tmin = (box.max[0] - origin[0]) * invdirx;
            tmax = (box.min[0] - origin[0]) * invdirx;

        }

        if (invdiry >= 0) {

            tymin = (box.min[1] - origin[1]) * invdiry;
            tymax = (box.max[1] - origin[1]) * invdiry;

        } else {

            tymin = (box.max[1] - origin[1]) * invdiry;
            tymax = (box.min[1] - origin[1]) * invdiry;

        }

        if ((tmin > tymax) || (tymin > tmax)) return null;

        if (tymin > tmin || isNaN(tmin)) tmin = tymin;

        if (tymax < tmax || isNaN(tmax)) tmax = tymax;

        if (invdirz >= 0) {

            tzmin = (box.min[2] - origin[2]) * invdirz;
            tzmax = (box.max[2] - origin[2]) * invdirz;

        } else {

            tzmin = (box.max[2] - origin[2]) * invdirz;
            tzmax = (box.min[2] - origin[2]) * invdirz;

        }

        if ((tmin > tzmax) || (tzmin > tmax)) return null;

        if (tzmin > tmin || tmin !== tmin) tmin = tzmin;

        if (tzmax < tmax || tmax !== tmax) tmax = tzmax;

        //return point closest to the ray (positive side)

        if (tmax < 0) return null;

        return this.at(tmin >= 0 ? tmin : tmax, target);
    }


}
