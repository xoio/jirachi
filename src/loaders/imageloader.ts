/**
 * Builds promises to load image assets
 * @param assets {Array<any>} an array of items. Can be just an array of strings or an Object. If an object, all the
 * items should have the "url" property in them.
 */
export default function (assets: Array<any>) {

    let proms = assets.map(path => {
        return new Promise((res, rej) => {

            let img = new Image()
            img.crossOrigin = "anonymous"

            let isObject = false
            if (path instanceof Object) {
                isObject = true
                if (path.hasOwnProperty("url")) {
                    img.src = path.url
                }
            } else {
                img.src = path
            }

            img.onload = () => {

                if (isObject) {
                    // don't need url anymore so remove.
                    delete path.url
                    res({
                        ...path,
                        img
                    })
                } else {
                    res(img)
                }
            }
            img.onerror = e => {
                rej(e)
            }
        })
    })

    return Promise.all(proms)
}