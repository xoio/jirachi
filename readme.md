Jirachi
===
A simple WebGL framework. It takes an unorthodox approach by trying to simply augment the native WebGL api and its 
various objects with useful additions and helpers. 

This has slowly been built and added onto over a number of years. You can see some samples built with older versions 
of the library [here](http://labs.xoio.co) 

Typing
===
As this augments existing types, there is a type definitions file at `src/types/index.d.ts`. This should provide 
visibility into the custom additions but at the moment may not all provide IDE completion with regard to arguments, etc.

Setup 
===
* `npm install` - installs Vite which is used as the build tool as well as the custom packages.
* `npm run build` to build the library. Library will get written out in the project root within `dist` Currently set 
  up to provide a JS and UMD module once built. ~~The library will also be automatically built with each push, you can find 
  the build artifacts [here](https://gitlab.com/xoio/jirachi/-/artifacts)~~ Running into a weird issue where relative paths aren't resolving in Gitlab's CI env. Going to revisit this later. 


Debugging
===
I'm debugging within the repo, using Vite and building with the file found at `app.ts`. Assume anything in there is in-progress.

Credits
===
This makes use of some third-party libraries to help things along including 
* [glMatrix](https://glmatrix.net/)
* [primitive-plane](https://github.com/vorg/primitive-plane)
* [primitive-cube](https://github.com/vorg/primitive-cube)
* [webgl-noise](https://github.com/ashima/webgl-noise)
* Various bits from [three.js](https://threejs.org/)
* Also, some functions have been adapted from various resources across the net; I will do my best to credit but in 
  case I forget, do feel free to let me know. I'm pretty much everywhere under @sortofsleepy
