import path from "path"
import typescript from "@rollup/plugin-typescript";
import {defineConfig} from "vite";

console.log("PATH IS", path.resolve(__dirname,"src/main.ts"))
export default defineConfig({
    build: {
        emptyOutDir: true,
        minify: false,
        rollupOptions: {

            plugins: [
                typescript()
            ]
        },
        lib: {
            name: "jra",
            formats: ["es"],
            entry: path.resolve(__dirname,"src/main.ts"),
            fileName: "jra"
        }
    }
})

